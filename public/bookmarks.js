// Note: having length != 4 will mess with layout based on how the site is styled
const bookmarks = [
  {
    title: "Main",
    links: [
      { name: "Mail", url: "https://inbox.google.com" },
      { name: "GitLab", url: "https://gitlab.com/thelinuxcast" },
      { name: "Drive", url: "https://drive.google.com" },
      { name: "Patreon", url: "https://patreon.com" },
      { name: "Anchor", url: "https://anchor.fm" },
      { name: "Facebook", url: "https://facebook.com" },
    ],
  },
  {
    title: "Social",
    links: [
      { name: "Youtube", url: "https://youtube.com" },
      { name: "Studio", url: "https://studio.youtube.com" },
      { name: "Tumblr", url: "https://tumblr.com" },
      { name: "Odysee", url: "https://odysee.com" },
      { name: "Twitch", url: "https://twitch.tv" },
    ],
  },
  {
    title: "Fanfiction",
    links: [
      { name: "Harmony AO3", url: "https://archiveofourown.org/tags/Hermione%20Granger*s*Harry%20Potter/works?page=1" },
      {
        name: "Percabeth AO3",
        url: "https://bit.ly/3OdwvJi",
      },
      { name: "West Wing AO3", url: "https://bit.ly/3Q9vkx2" },
      {
        name: "Harmony FFN",
        url: "https://www.fanfiction.net/book/Harry-Potter/?&srt=1&lan=1&r=10&c1=1&c2=3",
      },
      { name: "Flowerpot", url: "https://archiveofourown.org/tags/Fleur%20Delacour*s*Harry%20Potter/works" },
      { name: "Honks", url: "https://archiveofourown.org/tags/Harry%20Potter*s*Nymphadora%20Tonks/works" },
    ],
  },
  {
    title: "Tech",
    links: [
      { name: "The Verge", url: "https://theverge.com" },
      { name: "Thurrott", url: "https://thurrott.com" },
      { name: "Dedoimedo", url: "https://dedoimedo.com" },
      { name: "OMG!Ubuntu", url: "https://www.omgubuntu.co.uk/" },
      { name: "Ars Technica", url: "https://arstechnica.com" },
    ],
  },
  {
    title: "Misc",
    links: [
      { name: "BGN", url: "https://bleedinggreennation.com" },
      { name: "AWS", url: "https://aws.amazon.com/console/" },
      { name: "MonkeyType", url: "https://monkeytype.com" },
      { name: "OpenSuSE Forums", url: "https://forums.opensuse.org" },
    ],
  },
];
